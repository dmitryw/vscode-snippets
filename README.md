# VS Code Snippets

This repo is for group contributions to VS Code snippets, which can make writing code quicker and easier, especially redundant or repetetive code (think unit tests, what?).

### Here's the way to use these snippets so far...

* Clone this repo:
```
$ git clone git@gitlab.com:dmitryw/vscode-snippets.git
```

* Go into the newly created directory:
```
$ cd vscode-snippets
```

* Copy these files to the VS Code Application Support folder (this is the path that worked for me, but it may vary):
```
$ cp ./*.json ~/Library/Application\ Support/Code/User/snippets
```

### We'll probably have to have a conversation around how we want to share these versus tweaking them for our personal preference

I was thinking we might want to have a branch for our own changes, then we could probably mix and match between ourselves? Other ideas?

### More Info on snippets

Check out the [VS Code docs on snippets](https://code.visualstudio.com/docs/editor/userdefinedsnippets)
